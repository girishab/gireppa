CREATE TABLE Records
(
  visits bigint NOT NULL,
  website character varying(1000) NOT NULL,
  msgdate date NOT NULL,
  CONSTRAINT Records_info_pkey PRIMARY KEY (website,msgdate));

INSERT INTO records (visits, website, msgdate) VALUES
    (1406545, 'www.bing.com','2014-09-10'),
    (1983116,'www.ebay.com.au', '2013-09-10'),
    (10434720,'www.facebook.com', '2014-09-10'),
    (2153612,'mail.live.com', '2014-09-10'),
    (1326531,'www.wikipedia.org', '2014-09-10'),
    (2136612,'mail.live1.com', '2014-09-10'),
    (2536612,'mail.live2.com', '2014-09-10'),
    (1536612,'mail.live.com', '2014-09-11'),
    (3154653,'www.ebay.com.au', '2014-09-10'); 


select * from records where msgdate='2014-09-10' order by visits desc limit 5;